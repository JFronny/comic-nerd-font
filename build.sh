#!/bin/sh
set -e
./getvendor.sh
./preprocess.py
./vendor/nerd-fonts/font-patcher --complete --careful patched/comic-adjusted.ttf -ext ttf -out patched/
./vendor/nerd-fonts/font-patcher --complete --careful patched/comic-bold-adjusted.ttf -ext ttf -out patched/
