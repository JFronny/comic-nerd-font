#!/bin/sh
set -e
# Download Fonts
[ ! -f vendor/comic-shanns.otf ] && curl -L -o vendor/comic-shanns.otf "https://github.com/shannpersand/comic-shanns/blob/master/v2/comic%20shanns.otf?raw=true"
[ ! -f vendor/cousine-regular.ttf ] && curl -L -o vendor/cousine-regular.ttf "https://github.com/google/fonts/blob/main/apache/cousine/Cousine-Regular.ttf?raw=true"

# Download NerdFont patcher
[ ! -d vendor/nerd-fonts ] && git clone git@github.com:ryanoasis/nerd-fonts.git vendor/nerd-fonts || git -C vendor/nerd-fonts pull

