#!/bin/sh
set -e
[ ! -f "patched/Comic Nerd Font Complete.ttf" ] && ./build.sh
cp "patched/Comic Nerd Font Complete.ttf" ~/.local/share/fonts/
cp "patched/Comic Bold Nerd Font Complete.ttf" ~/.local/share/fonts/
