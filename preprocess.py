#!/bin/python
import sys
import fontforge
import psMat
import unicodedata

def height(font):
    return float(font.capHeight)

def adjust_height(source, template, scale):
    source.selection.all()
    source.transform(psMat.scale(height(template) / height(source)))
    for attr in ['ascent', 'descent',
                'hhea_ascent', 'hhea_ascent_add',
                'hhea_linegap',
                'hhea_descent', 'hhea_descent_add',
                'os2_winascent', 'os2_winascent_add',
                'os2_windescent', 'os2_windescent_add',
                'os2_typoascent', 'os2_typoascent_add',
                'os2_typodescent', 'os2_typodescent_add',
                ]:
        setattr(source, attr, getattr(template, attr))
    source.transform(psMat.scale(scale))

font = fontforge.open('vendor/comic-shanns.otf')
ref = fontforge.open('vendor/cousine-regular.ttf')

for g in font.glyphs():
    uni = g.unicode
    category = unicodedata.category(chr(uni)) if 0 <= uni <= sys.maxunicode else None
    if g.width > 0 and category not in ['Mn', 'Mc', 'Me']:
        target_width = 510
        if g.width != target_width:
            delta = target_width - g.width
            lsb = delta / 2
            g.left_side_bearing = int(g.left_side_bearing + lsb)
            g.right_side_bearing = int(g.right_side_bearing + lsb)
            g.width = target_width

font.familyname = 'Comic'
font.version = '0.1.1'
font.comment = 'https://github.com/dtinth/comic-mono-font'
font.copyright = 'https://github.com/dtinth/comic-mono-font/blob/master/LICENSE'

adjust_height(font, ref, 0.875)
font.sfnt_names = []
font.fontname = 'Comic'
font.fullname = 'Comic'
font.generate('patched/comic-adjusted.ttf')
font.copy()

font.selection.all()
font.fontname = 'Comic-Bold'
font.fullname = 'Comic Bold'
font.weight = 'Bold'
font.changeWeight(32, "LCG", 0, 0, "squish")
font.generate('patched/comic-bold-adjusted.ttf')
